//
//  main.cpp
//  Program-6
//
//  Created by Carmen Chan
//

#include "Subject.h"
#include "SchoolObserver.h"
#include "CreditObserver.h"
#include "BankObserver.h"
#include <iostream>

int main(int argc, const char * argv[])
{
    //create one instance of Subject and derived Observer classes
    Subject subject("800 W Campbell Rd, Richardson, TX 75080");
    SchoolObserver schoolObserver;
    CreditObserver creditObserver;
    BankObserver bankObserver;
    
    //register instances of Observer classes to Subject class
    subject.addObserver(schoolObserver);
    subject.addObserver(creditObserver);
    subject.addObserver(bankObserver);
    
    //make a change to Subject class
    subject.setAddress("2500 Victory Ave, Dallas, TX 75219");
    
    //deregister one of the Observer instances 
    subject.removeObserver(schoolObserver);
    
    //make another change in Subject class to display removal of Observer
    subject.setAddress("1000 Ballpark Way, Arlington, TX 76011");
    
    return 0;
}

