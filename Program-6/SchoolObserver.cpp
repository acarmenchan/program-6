//
//  SchoolObserver.cpp
//  Program-6
//
//  Created by Carmen Chan
//

#include "SchoolObserver.h"

void SchoolObserver::subjectChanged(string originalAdd, string newAddress)
{
    cout << "The SchoolObserver received an address change notification: " << endl;
    cout << endl;
    cout << "\tOld Address: " << originalAdd << endl;
    cout << "\tNew Address: " << newAddress << endl;
    cout << endl;
}