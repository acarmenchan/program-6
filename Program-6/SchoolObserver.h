//
//  SchoolObserver.h
//  Program-6
//
//  Created by Carmen Chan
//

#ifndef __Program_6__SchoolObserver__
#define __Program_6__SchoolObserver__

#include "AbstractObserver.h"
#include <iostream>

using namespace std;

//derived class
class SchoolObserver : public AbstractObserver
{
public:
    ~SchoolObserver() {}
    void subjectChanged(string, string);
};

#endif /* defined(__Program_6__SchoolObserver__) */
