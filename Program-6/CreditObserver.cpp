//
//  CreditObserver.cpp
//  Program-6
//
//  Created by Carmen Chan
//

#include "CreditObserver.h"

void CreditObserver::subjectChanged(string originalAdd, string newAddress)
{
    cout << "The CreditObserver received an address change notification: " << endl;
    cout << endl;
    cout << "\tOld Address: " << originalAdd << endl;
    cout << "\tNew Address: " << newAddress << endl;
    cout << endl;
}