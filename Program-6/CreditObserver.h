//
//  CreditObserver.h
//  Program-6
//
//  Created by Carmen Chan
//

#ifndef __Program_6__CreditObserver__
#define __Program_6__CreditObserver__

#include "AbstractObserver.h"
#include <iostream>

using namespace std;

//derived class
class CreditObserver : public AbstractObserver
{
public:
    ~CreditObserver() {}
    void subjectChanged(string, string);
};

#endif /* defined(__Program_6__CreditObserver__) */
