//
//  Subject.h
//  Program-6
//
//  Created by Carmen Chan
//

#ifndef __Program_6__Subject__
#define __Program_6__Subject__

#include "AbstractObserver.h"
#include <iostream>
#include <string>
#include <list>

using namespace std;

class Subject
{
private:
    string originalAddress;
    string address;
    list<AbstractObserver *> observers;
    void notify();
    
public:
    //constructor initializes address
    Subject(string _addr);
    
    //setter changes address
    void setAddress(string newAddress);
    string getAddress();
    
    //additional functions that allow other classes (the observers) to register and deregister themselves with this class
    void addObserver(AbstractObserver& observer);
    void removeObserver(AbstractObserver& observer);
};

#endif /* defined(__Program_6__Subject__) */
