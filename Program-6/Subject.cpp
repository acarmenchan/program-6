//
//  Subject.cpp
//  Program-6
//
//  Created by Carmen Chan
//

#include "Subject.h"

Subject::Subject(string _address)
{
    address = _address;
}

void Subject::setAddress(string newAddress)
{
    originalAddress = address;
    address = newAddress;
    notify();
}

void Subject::notify()
{
    //update Observers by iterating over list with registered callback functions
    list<AbstractObserver *>::iterator myIterator;
    
    for (myIterator = observers.begin(); myIterator != observers.end(); ++myIterator)
        (*myIterator)->subjectChanged(originalAddress, address);
}

string Subject::getAddress()
{
    return address;
}

void Subject::addObserver(AbstractObserver& observer)
{
    observers.push_back(&observer);
}

void Subject::removeObserver(AbstractObserver& observer)
{
    //iterate over list to find Observer and remove
    list<AbstractObserver *>::iterator myIterator;
    
    for (myIterator = observers.begin(); myIterator != observers.end(); ++myIterator)
    {
        if ((*myIterator) == &observer)
            observers.erase(myIterator);
    }
}