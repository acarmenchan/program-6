//
//  BankObserver.h
//  Program-6
//
//  Created by Carmen Chan
//

#ifndef __Program_6__BankObserver__
#define __Program_6__BankObserver__

#include "AbstractObserver.h"
#include <iostream>

using namespace std;

//derived class
class BankObserver : public AbstractObserver
{
public:
    ~BankObserver() {}
    void subjectChanged(string, string);
};

#endif /* defined(__Program_6__BankObserver__) */
