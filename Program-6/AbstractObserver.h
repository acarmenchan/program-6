//
//  AbstractObserver.h
//  Program-6
//
//  Created by Carmen Chan
//

#ifndef __Program_6__AbstractObserver__
#define __Program_6__AbstractObserver__

#include <iostream>

using namespace std;

//base class
class AbstractObserver
{
public:
    virtual ~AbstractObserver(){}
    virtual void subjectChanged(string, string)=0;
    
};

#endif /* defined(__Program_6__AbstractObserver__) */
