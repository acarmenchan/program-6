//
//  BankObserver.cpp
//  Program-6
//
//  Created by Carmen Chan
//

#include "BankObserver.h"

void BankObserver::subjectChanged(string originalAdd, string newAddress)
{
    cout << "The BankObserver received an address change notification: " << endl;
    cout << endl;
    cout << "\tOld Address: " << originalAdd << endl;
    cout << "\tNew Address: " << newAddress << endl;
    cout << endl;
}